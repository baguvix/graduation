#include "auth_manager_plugin.h"

#include "auth.h"

#include <qqml.h>

void AuthManagerPlugin::registerTypes(const char *uri)
{
    // @uri Auth
    qmlRegisterType<AuthManager>(uri, 1, 0, "AuthManager");
}

