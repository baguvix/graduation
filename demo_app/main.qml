import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import ru.omprussia.auth 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 300
    height: 420
    title: qsTr("Demo Notes")

    property string stored_pass: ""
    property string entered_pass: ""
    //AuthManager {
    //    id: auth_agent
    //
    //    onAuthenticatioEnded: {
    //        if (!status)
    //        {
    //            stack.push("notes_mainscreen.qml")
    //        }
    //    }
    //}

    StackView {
        id: stack
        initialItem: init_screen
        anchors.fill: parent
    }

    Page {
        id: init_screen
        anchors.fill: parent

        Label {
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 80

            text: "Welcome"
        }

        Button {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 5
            anchors.leftMargin: 5
            width: 50
            height: 50

            text: "• • •"

            onClicked: {
                stack.push("settings.qml")
            }
        }

        RowLayout {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 25

            Button {
                anchors.bottomMargin: 100
                text: "Login"

                onClicked: {
                    log = stack.push("input_control.qml")
                }
            }
        }
    }

}
