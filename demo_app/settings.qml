import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import ru.omprussia.auth 1.0

Page {
    id: settings_screen
    visible: true
    width: 300
    height: 420
    title: qsTr("Demo Notes")

    //AuthManager {
    //    id: enroll_agent
    //
    //    onEnrollmentEnded: {
    //        stack.pop()
    //    }
    //}

    RowLayout {
        id: back_button
        anchors.top: parent.top

        Button {
            //anchors.left: parent.left
            //anchors.top: parent.top
            Layout.topMargin: 5
            Layout.leftMargin: 5

            text: "Back"
            onClicked: {
                root.stored_pass = root.entered_pass;
                console.log(root.stored_pass)
                stack.pop()
            }
        }
        Label {
            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.top: parent.top
            Layout.topMargin: 15
            Layout.leftMargin: 30
            text: "Settings"
        }

    }

    ListModel {
        id: authenticators
        ListElement {
            description: "Password"
            property int number: 0
            property bool state: true
        }
    }

    ListView {
        anchors.top: back_button.bottom
        anchors.topMargin: 40
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - 20
        height: parent.height - 50

        model: authenticators
        delegate: Item {
            RowLayout {
                Switch {
                    Layout.rightMargin: 20
                    checked: root.stored_pass === "" ? false : true
                    onToggled: {
                        if (position)
                        {
                            stack.push("input_control.qml")
                        }
                        else
                        {
                            root.stored_pass = ""
                        }
                    }
                }
                Label {
                    text: description
                }
            }
        }
    }

}
