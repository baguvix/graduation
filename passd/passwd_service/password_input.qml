import QtQuick 2.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14


Page {
    id: root
    visible: true
    width: 300
    height: 420
    title: qsTr("Passwd Input")


    GridLayout {
        id: grid; columns: 3

        Button { text: "1" }
        Button { text: "2" }
        Button { text: "3" }
        Button { text: "4" }
        Button { text: "5" }
        Button { text: "6" }
        Button { text: "7" }
        Button { text: "8" }
        Button { text: "9" }
        Button { visible: false }
        Button { text: "0" }
        Button { visible: false }

    }

}
