#include "passwd.h"
#include "passwd_adaptor.h"
#include "input_control.h"
#include <QCoreApplication>

#define     DBUS_ADDR   "unix:abstract=auth_bus"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QDBusServer bus(DBUS_ADDR, &a);
    bus.setAnonymousAuthenticationAllowed(true);
    QObject::connect(&bus, &QDBusServer::newConnection, [=](const QDBusConnection &connection){
        Passwd *service = new Passwd;
        new PasswdAdaptor(service);

        qDebug() << "[Password Service] Connection accepted!\n";
        QDBusConnection conn(connection);
        conn.registerObject("/passwd", service);
        qDebug() << "[Password Service] Connection established: " << conn.isConnected();
    });

    return a.exec();
}
