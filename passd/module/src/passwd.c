#define PAM_SM_AUTH
#define PAM_SM_PASSWORD

#include <stdlib.h>
#include <security/pam_modules.h>
#include <security/pam_appl.h>
#include <security/pam_ext.h>
#include <dbus/dbus.h>
#include <stdio.h>
#include <string.h>

#define		DBUS_ADDR	"unix:abstract=auth_bus"
#define		DBUS_NAME	"ru.omprussia.passwd"
#define		DBUS_OBJ	"/passwd"
#define		DBUS_IFACE	"ru.omprussia.passwd"
#define		AUTH_METHOD	"authenticate"
#define		ENROLL_METHOD	"enrollment"
#define		CHANGE_METHOD	"chauthtok"

PAM_EXTERN int	pam_sm_authenticate(pam_handle_t	*pamh,
           		            int			flags,
                    		    int			argc,
            		            const char		**argv)
{
	(void)(flags);
	(void)(argc);
	(void)(argv);
	
	DBusConnection		*conn;
	DBusMessage		*reply;
	DBusMessage		*msg;
	DBusMessageIter		iter;
	DBusError		error;
	int			rc;
	
	const char	*user	= NULL;

	printf("[Module] Auth initiated!\n");
	dbus_error_init(&error);
	conn = dbus_connection_open(DBUS_ADDR, &error);
	if (conn == NULL)
	{
		fprintf (stderr, "Failed to open connection to 'auth_bus' bus: %s\n",
			error.message);
		dbus_error_free (&error);
		return PAM_AUTH_ERR;
	}

	if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS
		|| user == NULL
		|| *user == '\0')
		return PAM_USER_UNKNOWN;
	msg = dbus_message_new_method_call(NULL,
                                           DBUS_OBJ,
                                           DBUS_IFACE,
                                           AUTH_METHOD);
	if (msg == NULL)
	{
		fprintf (stderr, "Couldn't allocate D-Bus message\n");
		return PAM_AUTH_ERR;
	}

	dbus_message_iter_init_append(msg, &iter);
	dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &user);
	
	reply = dbus_connection_send_with_reply_and_block (conn,
                                                           msg, -1,
                                                           &error);
	if (reply == NULL)
	{
		fprintf(stderr, "Error invoking authenticate: %s : %s\n", error.name, error.message);
		dbus_error_free (&error);
		return PAM_AUTH_ERR;
	}
	
	dbus_message_iter_init(reply, &iter);
	dbus_message_iter_get_basic(&iter, &rc);

	if (rc)
	{
		printf("[Module] Authentication succeed\n");
		return PAM_SUCCESS;
	}
	return PAM_AUTH_ERR;
}

PAM_EXTERN int	pam_sm_setcred	(pam_handle_t	*pamh,
            			 int		flags,
            			 int		argc,
             			 const char	**argv)
{
    (void)(pamh);
    (void)(flags);
    (void)(argc);
    (void)(argv);
    return (PAM_SUCCESS);
}

PAM_EXTERN int	pam_sm_chauthtok(pam_handle_t	*pamh,
				 int		flags,
				 int		argc,
				 const char	**argv)
{
	(void)(flags);
	(void)(argc);
	(void)(argv);
	
	DBusConnection		*conn;
	DBusMessage		*reply;
	DBusMessage		*msg;
	DBusMessageIter		iter;
	DBusError		error;
	int			rc;
	
	const char	*user	= NULL;
	const char	*op	= NULL;

	if (flags & PAM_PRELIM_CHECK)
		return PAM_SUCCESS;

	dbus_error_init(&error);
	conn = dbus_connection_open(DBUS_ADDR, &error);
	if (conn == NULL)
	{
		fprintf (stderr, "Failed to open connection to 'auth_bus' bus: %s\n",
			error.message);
		dbus_error_free (&error);
		return PAM_AUTH_ERR;
	}

	if (pam_get_user(pamh, &user, NULL) != PAM_SUCCESS
		|| user == NULL
		|| *user == '\0')
		return PAM_USER_UNKNOWN;

	op = pam_getenv(pamh, "OP");
	if (!strncmp(op, "ENROLL", 6))
	{
		printf("[Module] Enroll initiated!\n");
		msg = dbus_message_new_method_call(NULL,
                                           DBUS_OBJ,
                                           DBUS_IFACE,
                                           ENROLL_METHOD);
	}
	else
	{
		printf("[Module] Auth material change initiated!\n");
		msg = dbus_message_new_method_call(NULL,
                                           DBUS_OBJ,
                                           DBUS_IFACE,
                                           CHANGE_METHOD);
	}

	if (msg == NULL)
	{
		fprintf (stderr, "Couldn't allocate D-Bus message\n");
		return PAM_AUTH_ERR;
	}

	dbus_message_iter_init_append(msg, &iter);
	dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &user);
	
	reply = dbus_connection_send_with_reply_and_block (conn,
                                                           msg, -1,
                                                           &error);
	if (reply == NULL)
	{
		fprintf(stderr, "Error invoking %s: %s : %s\n", op, error.name, error.message);
		dbus_error_free (&error);
		return PAM_AUTH_ERR;
	}
	
	dbus_message_iter_init(reply, &iter);
	dbus_message_iter_get_basic(&iter, &rc);

	if (rc)
	{
		printf("[Module] Operation %s succeed\n", op);
		return PAM_SUCCESS;
	}
	return PAM_AUTH_ERR;

}
