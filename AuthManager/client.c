#include <stdlib.h>
#include <stdio.h>
#include "authapi.h"

int main(int argc, char *argv[])
{
	char    **auth_list = NULL;
	int	ret_status = OK;
	int	i = 0;

	ret_status = authapi_get_authenticator_list(&auth_list);
	printf("authapi_get_authenticator_list(): \treturn status: |%s|\n", pauth_error(ret_status));
	if (ret_status != OK)
        	exit(EXIT_FAILURE);
	while (auth_list[i])
	{
		printf("\nAuthenticator number: %d\nAuthenticator description: %s\n\n", i, auth_list[i]);
		++i;
	}
	
	i = 0;
	ret_status = authapi_enrollment("grisha", i);
	printf("authapi_enrollment(): \t\t\treturn status: |%s|\n", pauth_error(ret_status));
	if (ret_status != OK)
		exit(EXIT_FAILURE);
	ret_status = authapi_authentication("grisha", SYSTEM_APP);
	printf("authapi_authentication(): \t\treturn status: |%s|\n", pauth_error(ret_status));
	if (ret_status != OK)
        	exit(EXIT_FAILURE);
	ret_status = authapi_change_authtok("grisha", i);
	printf("authapi_change_authtok(): \t\treturn status: |%s|\n", pauth_error(ret_status));
	if (ret_status != OK)
        	exit(EXIT_FAILURE);
	ret_status = authapi_set_authenticator_state("grisha",
                                                 SYSTEM_APP,
                                                 i,
                                                 FALSE);
	printf("authapi_set_authenticator_state(): \treturn status: |%s|\n", pauth_error(ret_status));
	if (ret_status != OK)
		exit(EXIT_FAILURE);

	return 0;
}
