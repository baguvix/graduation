#ifndef AUTH_API
# define AUTH_API

#include <glib.h>

#define SYSTEM_APP	"system"

enum	auth_error {
	OK = 0,
	BAD_USER_ID_FIELD,
	BAD_USER_ID,
	UNREGISTRED_USER,
	BAD_APP_ID_FIELD,
	BAD_APP_ID,
	BAD_AUTHENTICATOR_NUMBER,
	UNKNOWNERR,
	INTERNAL_ERR
};

char	*auth_error_descriptions[] = {
    "Success",
    "User identifier field shouldn`t be (null)",
    "Incorrect user identifier",
    "Specified user identifier is not registred",
    "Application identifier field shouldn`t be (null)",
    "Incorrect application identifier",
    "Bad authenticator slot number",
    "Error unknown..."
};

int	authapi_authentication(char *user_name, char *app_name);
int	authapi_enrollment(char *user_name, gint authenticator);
int	authapi_set_authenticator_state(char *user_name, char *app_name, gint authenticator, gboolean state);
int	authapi_get_authenticator_list(char ***list);
int	authapi_change_authtok(char *user_name, gint authenticator);
char	*pauth_error(gint error);

#endif
