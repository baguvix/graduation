TEMPLATE = lib
TARGET = auth_manager
QT += qml quick core
CONFIG += plugin c++11

TARGET = $$qtLibraryTarget($$TARGET)
uri = Auth

# Input
SOURCES += \
        auth_manager_plugin.cpp \
        auth.cpp

HEADERS += \
        auth_manager_plugin.h \
        auth.h \
        auth_api.h

DISTFILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) "$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)" "$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    INSTALLS += target qmldir
}

unix:!macx: LIBS += -L$$PWD/../AuthManager/auth_api/ -lauth_api

INCLUDEPATH += $$PWD/../AuthManager/auth_api
DEPENDPATH += $$PWD/../AuthManager/auth_api
