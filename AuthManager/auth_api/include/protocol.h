

#ifndef AUTH_PROT
# define AUTH_PROT

enum	operation
{
	ENROLLMENT = 1,
	AUTHENTICATION,
	SET_AUTH_STATE,
	GET_AUTH_LIST,
	CHANGE_AUTHTOK,
	UNKNOWN
};

typedef struct	query_s
{
	enum operation	op;
	gint		authenticator_number;
	gint		user_name_len;
	gint		app_name_len;
	gchar		*user_name;
	gchar		*app_name;
	gboolean	state;

}		query_t;


#endif
