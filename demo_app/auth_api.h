#ifndef AUTH_API
# define AUTH_API

#ifdef __cplusplus
extern "C" {
#endif

int	api_authentication(char *user_name, char *app_name);
int	api_enrollment(char *user_name, int authenticator);

#ifdef __cplusplus
}
#endif

#endif
