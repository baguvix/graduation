#ifndef AUTH_H
#define AUTH_H

#include <QQuickItem>
#include <QDir>
#include "auth_api.h"

class AuthManager : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(AuthManager)

public:
    explicit AuthManager(QQuickItem *parent = nullptr);
    ~AuthManager() override;

    void authenticate(void);
    void enrollment(void);

signals:
    void    authenticatioEnded(int status);
    void    enrollmentEnded(int status);

};

#endif // AUTH_H
