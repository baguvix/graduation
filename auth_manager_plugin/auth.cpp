#include "auth.h"

AuthManager::AuthManager(QQuickItem *parent)
    : QQuickItem(parent)
{
    // By default, QQuickItem does not draw anything. If you subclass
    // QQuickItem to create a visual item, you will need to uncomment the
    // following line and re-implement updatePaintNode()

    // setFlag(ItemHasContents, true);
}

AuthManager::~AuthManager()
{
}

void AuthManager::authenticate(void)
{
    QString username;
    QString app_identity;
    int     status;

    // the way we getting application identity here
    app_identity = "demo_app";

    // the way we should be sure with caller identity
    username = QDir::home().dirName();

    status = api_authentication(username.toLocal8Bit().data(), app_identity.toLocal8Bit().data());

    emit authenticatioEnded(status);
}

void AuthManager::enrollment(void)
{
    QString username;
    qint32  authenticator = 1;
    int status;

    username = QDir::home().dirName();

    status = api_enrollment(username.toLocal8Bit().data(), authenticator);

    emit enrollmentEnded(status);
}
