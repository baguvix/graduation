#include "passwd.h"
#include "input_control.h"

PasswordInput::PasswordInput(QObject *parent): QObject(parent)
{
}

QString PasswordInput::read_pass()
{
    QTextStream in(stdin);
    QString pass;

    pass = in.readLine();
    return pass;
}

Passwd::Passwd(QObject *parent) : PasswordInput(parent)
{
}

void Passwd::setup_db()
{
    QFile inFile("/home/bbc/Documents/graduation/passd/database.json");
    inFile.open(QIODevice::ReadOnly|QIODevice::Text);
    QByteArray data = inFile.readAll();
    inFile.close();

    QJsonParseError errorPtr;
    database = QJsonDocument::fromJson(data, &errorPtr);
    if (database.isNull()) {
        qDebug() << "Parse failed";
    }
}

void Passwd::close_db()
{
    QFile outFile("/home/bbc/Documents/graduation/passd/database.json");
    outFile.open(QFile::WriteOnly);
    outFile.write(database.toJson());
    outFile.close();
}

int Passwd::authenticate(QString username)
{
    qDebug() << "[Password Service] Authentication in progress...";
    setup_db();
    QJsonObject rootObj = database.object();
    QString stored_digest;
    int     ret = 1;

    QCryptographicHash pass_hash(QCryptographicHash::Sha256);
    if (rootObj.find(username) != rootObj.end())
        stored_digest = rootObj.find(username).value().toString();
    else
    {
        qDebug() << "[Password Service] Unregistred user!";
        return (0);
    }
    int trys = 0;
    while (trys < 3)
    {
        pass_hash.reset();
        qDebug() << "[Password Service] Enter pass:";
        QString new_pass = read_pass();
        pass_hash.addData(new_pass.toUtf8());
        qDebug() << "[Password Service] Entered: " << new_pass;
        if (stored_digest == QString(pass_hash.result()))
        {
            qDebug() << "[Password Service] Authentication completed!";
            trys = 3;
            ret = 1;
        }
        else
        {
            qDebug() << "[Password Service] Wrong pass! Try again!\n";
            ret = 0;
        }
        ++trys;
    }
    close_db();
    return (ret);
}

int Passwd::enrollment(QString username)
{
    qDebug() << "[Password Service] Enrollment in progresss...\n"
                "[Password Service] Enter new pass:";
    setup_db();
    QString new_pass = read_pass();
    QJsonObject rootObj = database.object();

    QCryptographicHash pass_hash(QCryptographicHash::Sha256);
    pass_hash.addData(new_pass.toUtf8());
    rootObj.insert(username, QJsonValue(QString(pass_hash.result())));
    qDebug() << "[Password Service] New pass is " << new_pass << ", got it!";
    database.setObject(rootObj);
    close_db();
    return (1);
}

int Passwd::chauthtok(QString username)
{
    qDebug() << "[Password Service] Authentication material change in progress...";
    setup_db();
    QJsonObject rootObj = database.object();
    QString stored_digest;
    int ret = 1;

    QCryptographicHash pass_hash(QCryptographicHash::Sha256);
    if (rootObj.find(username) != rootObj.end())
        stored_digest = rootObj.find(username).value().toString();
    else
    {
        qDebug() << "[Password Service] Unregistred user!";
        return (0);
    }
    int trys = 0;
    while (trys < 3)
    {
        pass_hash.reset();
        qDebug() << "[Password Service] Enter old pass:";
        QString old_pass = read_pass();
        pass_hash.addData(old_pass.toUtf8());
        if (stored_digest == QString(pass_hash.result()))
        {
            qDebug() << "[Password Service] Enter new pass:";
            QString new_pass = read_pass();
            pass_hash.reset();
            pass_hash.addData(new_pass.toUtf8());
            rootObj.insert(username, QJsonValue(QString(pass_hash.result())));
            qDebug() << "[Password Service] New pass is " << new_pass << ", got it!";
            trys = 3;
            ret = 1;
        }
        else
        {
            qDebug() << "[Password Service] Wrong old pass! Try again!\n";
            ret = 0;
        }
        ++trys;
    }
    database.setObject(rootObj);
    close_db();
    return (ret);
}
