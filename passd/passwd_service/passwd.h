#ifndef PASSWD_H
#define PASSWD_H

#include <QObject>
#include <QApplication>
#include <QtDBus/QDBusAbstractAdaptor>
#include <QDBusServer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>
#include <QCryptographicHash>
#include "input_control.h"

class PasswordInput : public QObject
{
public:
    explicit PasswordInput(QObject *parent = nullptr);

    QString read_pass();
};

class Passwd : public PasswordInput
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "ru.omprussia.passwd")

public:
    explicit Passwd(QObject *parent = nullptr);

public slots:
    int     authenticate(QString username);
    int     enrollment(QString username);
    int     chauthtok(QString username);

private:
    void    setup_db();
    void    close_db();

    QJsonDocument     database;
    //QString         m_user;
    //QString         m_pass;
};

#endif // PASSWD_H
