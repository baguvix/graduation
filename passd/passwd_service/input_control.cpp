#include "input_control.h"

InputControl::InputControl(QQuickItem *parent) : QQuickItem(parent)
{
    m_active = false;
}

void    InputControl::setActive(bool value)
{
    m_active = value;
}

bool    InputControl::active(void)
{
    return m_active;
}
