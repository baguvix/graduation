#include <glib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "authapi.h"
#include "protocol.h"

#define SOCKET_PATH	"auth_server"

gint	build_packet(query_t *q, void **p)
{
	char		*packet = NULL;
	gint		len, offset;

	switch (q->op)
	{
		case UNKNOWN:
			len = -1;
			break;
		case AUTHENTICATION:
			len = sizeof(enum operation)
				+ sizeof(gint) + q->user_name_len
				+ sizeof(gint) + q->app_name_len;
			packet = g_try_malloc0(len);

			offset = 0;
			
			len = sizeof(enum operation);
			memcpy(packet + offset, &q->op, len);
			offset += len;

			len = sizeof(gint);
			memcpy(packet + offset, &q->user_name_len, len);
			offset += len;

			len = q->user_name_len;
			memcpy(packet + offset, q->user_name, len);
			offset += len;

			len = sizeof(gint);
			memcpy(packet + offset, &q->app_name_len, len);
			offset += len;

			len = q->app_name_len;
			memcpy(packet + offset, q->app_name, len);

			len += offset;
			break;
		case ENROLLMENT:
			len = sizeof(q->op)
				+ sizeof(q->user_name_len) + q->user_name_len
				+ sizeof(q->authenticator_number);
			packet = g_try_malloc0(len);

			offset = 0;
			
			len = sizeof(q->op);
			memcpy(packet + offset, &q->op, len);
			offset += len;

			len = sizeof(q->user_name_len);
			memcpy(packet + offset, &q->user_name_len, len);
			offset += len;

			len = q->user_name_len;
			memcpy(packet + offset, q->user_name, len);
			offset += len;

			len = sizeof(q->authenticator_number);
			memcpy(packet + offset, &q->authenticator_number, len);

			len += offset;
			break;
		case SET_AUTH_STATE:
			len = sizeof(q->op)
				+ sizeof(q->user_name_len) + q->user_name_len
				+ sizeof(q->app_name_len) + q->app_name_len
				+ sizeof(q->authenticator_number)
				+ sizeof(q->state);
			packet = g_try_malloc0(len);

			offset = 0;
			
			len = sizeof(q->op);
			memcpy(packet + offset, &q->op, len);
			offset += len;

			len = sizeof(q->user_name_len);
			memcpy(packet + offset, &q->user_name_len, len);
			offset += len;

			len = q->user_name_len;
			memcpy(packet + offset, q->user_name, len);
			offset += len;

			len = sizeof(q->app_name_len);
			memcpy(packet + offset, &q->app_name_len, len);
			offset += len;

			len = q->app_name_len;
			memcpy(packet + offset, q->app_name, len);
			offset += len;

			len = sizeof(q->authenticator_number);
			memcpy(packet + offset, &q->authenticator_number, len);
			offset += len;

			len = sizeof(q->state);
			memcpy(packet + offset, &q->state, len);

			len += offset;
			break;
		case GET_AUTH_LIST:
			len = sizeof(q->op);

			packet = g_try_malloc0(len);

			len = sizeof(q->op);
			memcpy(packet, &q->op, len);
			break;
		case CHANGE_AUTHTOK:
			len = sizeof(q->op)
				+ sizeof(q->user_name_len) + q->user_name_len
				+ sizeof(q->authenticator_number);
			packet = g_try_malloc0(len);

			offset = 0;
			
			len = sizeof(q->op);
			memcpy(packet + offset, &q->op, len);
			offset += len;

			len = sizeof(q->user_name_len);
			memcpy(packet + offset, &q->user_name_len, len);
			offset += len;

			len = q->user_name_len;
			memcpy(packet + offset, q->user_name, len);
			offset += len;

			len = sizeof(q->authenticator_number);
			memcpy(packet + offset, &q->authenticator_number, len);

			len += offset;
			break;
	}

	*p = packet;
	return (len);
}

int	authapi_authentication(char *user_name, char *app_name)
{
	struct sockaddr_un	addr;
  	char			buf[1024] = {};
  	int			fd;
	gint			size;
	query_t			query = {};
	gpointer		packet;

	if (user_name == NULL)
		return BAD_USER_ID_FIELD;
	if (strlen(user_name) == 0)
		return BAD_USER_ID;
	if (app_name == NULL)
		return BAD_APP_ID_FIELD;
	if (strlen(app_name) == 0)
		return BAD_APP_ID;

	if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	*addr.sun_path = 0;
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof(addr.sun_path) - 2);

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		perror("connect() error");
		exit(-1);
	}

	query.op = AUTHENTICATION;
	query.user_name = g_strdup(user_name);
	query.user_name_len = strlen(user_name);
	query.app_name = g_strdup(app_name);
	query.app_name_len = strlen(app_name);

	size = build_packet(&query, &packet);
	send(fd, packet, size, 0);
	recv(fd, buf, 1024, 0);
	gint rc = *(gint *)buf;

	close(fd);
	return (rc);
}

int	authapi_enrollment(char *user_name, gint authenticator)
{
	struct sockaddr_un	addr;
  	char			buf[1024] = {};
  	int			fd;
	gint			size;
	query_t			query = {};
	gpointer		packet;

	if (user_name == NULL)
		return BAD_USER_ID_FIELD;
	if (strlen(user_name) == 0)
		return BAD_USER_ID;

	if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	*addr.sun_path = '\0';
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof(addr.sun_path) - 2);

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		perror("connect() error");
		exit(-1);
	}

	query.op = ENROLLMENT;
	query.user_name = g_strdup(user_name);
	query.user_name_len = strlen(user_name);
	query.authenticator_number = authenticator;

	size = build_packet(&query, &packet);

	send(fd, packet, size, 0);
	recv(fd, buf, 1024, 0);
	gint rc = *(gint *)buf;

	close(fd);
	return (rc);
}

int	authapi_set_authenticator_state(char *user_name, char *app_name, gint authenticator, gboolean state)
{
	struct sockaddr_un	addr;
  	char			buf[1024] = {};
  	int			fd;
	gint			size;
	query_t			query = {};
	gpointer		packet;

	if (user_name == NULL)
		return BAD_USER_ID_FIELD;
	if (strlen(user_name) == 0)
		return BAD_USER_ID;
	if (app_name == NULL)
		return BAD_APP_ID_FIELD;
	if (strlen(app_name) == 0)
		return BAD_APP_ID;

	if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	*addr.sun_path = '\0';
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof(addr.sun_path) - 2);

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		perror("connect() error");
		exit(-1);
	}

	query.op = SET_AUTH_STATE;
	query.user_name = g_strdup(user_name);
	query.user_name_len = strlen(user_name);
	query.app_name = g_strdup(app_name);
	query.app_name_len = strlen(app_name);
	query.authenticator_number = authenticator;
	query.state = state;

	size = build_packet(&query, &packet);

	send(fd, packet, size, 0);
	recv(fd, buf, 1024, 0);
	gint rc = *(gint *)buf;

	close(fd);
	return (rc);
}

int	authapi_get_authenticator_list(char ***list)
{
	struct sockaddr_un	addr;
  	char			buf[1024] = {};
  	int			fd;
	gint			size;
	query_t			query = {};
	gpointer		packet;

	if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	*addr.sun_path = '\0';
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof(addr.sun_path) - 2);

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		perror("connect() error");
		exit(-1);
	}

	query.op = GET_AUTH_LIST;

	size = build_packet(&query, &packet);

	send(fd, packet, size, 0);
	recv(fd, buf, 1024, 0);

	gint	num = 0;
	char	*tmp = buf;
	size = *(gint *)tmp;
	while (size)
	{
		++num;
		tmp += sizeof(gint) + size;
		size = *(gint *)tmp;
	}

	*list = g_try_malloc0(++num * sizeof(char *));
	num = 0;
	tmp = buf;
	size = *(gint *)tmp;
	while (size)
	{
		tmp += sizeof(gint);
		
		if (((*list)[num] = g_strndup(tmp, size)) == NULL)
		{
			g_print("Error g_strndup: NULL\n");
			exit(EXIT_FAILURE);
		}
		
		++num;
		tmp += size;
		size = *(gint *)tmp;
	}
	(*list)[num] = NULL;

	close(fd);
	return (OK);
}

int	authapi_change_authtok(char *user_name, gint authenticator)
{
	struct sockaddr_un	addr;
  	char			buf[1024] = {};
  	int			fd;
	gint			size;
	query_t			query = {};
	gpointer		packet;

	if (user_name == NULL)
		return BAD_USER_ID_FIELD;
	if (strlen(user_name) == 0)
		return BAD_USER_ID;

	if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1)
	{
		perror("socket() error");
		exit(-1);
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	*addr.sun_path = '\0';
	strncpy(addr.sun_path + 1, SOCKET_PATH, sizeof(addr.sun_path) - 2);

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		perror("connect() error");
		exit(-1);
	}

	query.op = CHANGE_AUTHTOK;
	query.user_name = g_strdup(user_name);
	query.user_name_len = strlen(user_name);
	query.authenticator_number = authenticator;

	size = build_packet(&query, &packet);

	send(fd, packet, size, 0);
	recv(fd, buf, 1024, 0);
	gint rc = *(gint *)buf;

	close(fd);
	return (rc);

}

char	*pauth_error(gint error)
{
	if (error > UNKNOWNERR)
		error = UNKNOWNERR;
	return auth_error_descriptions[error];
}
