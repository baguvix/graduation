import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Page {
    id: main_screen
    visible: true
    width: 300
    height: 420
    title: qsTr("Demo Notes")

    Button {
        id: logout_button
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.leftMargin: 5

        text: "Logout"
        onClicked: {
            stack.pop()
            stack.pop()
        }
    }

    TextArea {
        anchors.top: logout_button.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        placeholderText: "Secret info!"
        bottomInset: 5
        background: Rectangle {
            radius: 10
            color: "cyan"
            border.color: "black"
        }

        width: main_screen.width - 40
        height: 200
    }

}
