#include <glib.h>
#include <glib/gstdio.h>
#include <glib-unix.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <string.h>
#include "authapi.h"
#include "protocol.h"


/*----------------------------------------------------------------------------*/
#define	SOCK_ADDR		" auth_server"
#define DBUS_ADDR		"unix:abstract=auth_bus"
#define BUF_LEN			1024

typedef struct			server_s
{
	gint			sock_fd;
	struct sockaddr_un	addr;
//	GDBusServer		*bus;

}				server_t;
/*----------------------------------------------------------------------------*/

static GMainLoop *main_loop	= NULL;

int	setup_server(server_t *serv)
{
	GError	*error;
	gchar	*bus_guid;

	serv->sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);

	serv->addr.sun_family = AF_UNIX;
	strncpy(serv->addr.sun_path, SOCK_ADDR, sizeof(serv->addr.sun_path) - 1);
	serv->addr.sun_path[0] = '\0';
	bind(serv->sock_fd, (struct sockaddr *)&serv->addr, sizeof(serv->addr));
	listen(serv->sock_fd, 1);

	// dbus
/*	error = NULL;
	bus_guid = g_dbus_generate_guid();
	serv->bus = g_dbus_server_new_sync(DBUS_ADDR, G_DBUS_SERVER_FLAGS_NONE, bus_guid,
					   NULL, NULL, &error);
	g_dbus_server_start(serv->bus);
	if (serv->bus == NULL)
	{
		g_printerr ("Error creating server at address %s\n", DBUS_ADDR, error->message);
		g_error_free (error);
		return (1);
	}
	
	g_free(bus_guid);
*/

	return (0);
}

void	close_server(server_t *serv)
{
	close(serv->sock_fd);
//	g_object_unref(serv->bus);
}

int	parse_query(char *data, query_t *q)
{
	gint	len;

	q->op = *((enum operation *)data);
	data += sizeof(enum operation);

	switch (q->op)
	{
		case ENROLLMENT:
			q->user_name_len = *((gint *)data);
			data += sizeof(gint);
			q->user_name = g_strndup(data, q->user_name_len);
			data += q->user_name_len;
			q->authenticator_number = *((gint *)(data));
			break;
		case AUTHENTICATION:
			q->user_name_len = *((gint *)data);
			data += sizeof(gint);
			q->user_name = g_strndup(data, q->user_name_len);
			data += q->user_name_len;
			q->app_name_len = *((gint *)data);
			data += sizeof(gint);
			q->app_name = g_strndup(data, q->app_name_len);
			break;
		case SET_AUTH_STATE:
			q->user_name_len = *((gint *)data);
			data += sizeof(gint);
			q->user_name = g_strndup(data, q->user_name_len);
			data += q->user_name_len;
			q->app_name_len = *((gint *)data);
			data += sizeof(gint);
			q->app_name = g_strndup(data, q->app_name_len);
			data += q->app_name_len;
			q->authenticator_number = *((gint *)(data));
			data += sizeof(gint);
			q->state = *((gboolean *)data);
			break;
		case CHANGE_AUTHTOK:
			q->user_name_len = *((gint *)data);
			data += sizeof(gint);
			q->user_name = g_strndup(data, q->user_name_len);
			data += q->user_name_len;
			q->authenticator_number = *((gint *)(data));
			break;
		case GET_AUTH_LIST:
			break;
		default:
			q->op = UNKNOWN;
	}
	
	g_print("[server] query fields:\n\t op_code: %d\n\t auth_num: %d\n\t user_name_len: %d\n\t user_name: %s\n\t app_name_len: %d\n\t app_name: %s\n", q->op, q->authenticator_number, q->user_name_len, q->user_name, q->app_name_len, q->app_name);
	return (0);
}

gint	get_auth_count(void)
{
	GDir	*dir;
	GError	*error = NULL;
	gchar	*dentry;
	gchar	*nump;
	gint	num;
	gint	max_num = 0;

	dir = g_dir_open("/etc/pam.d/", 0, &error);
	if (dir == NULL)
	{
		g_printerr("Error on g_dir_open '/etc/pam.d/', error:%s\n", error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}
	while ((dentry = g_dir_read_name(dir)))
	{
		gchar	*tmp = NULL;
		if (g_strrstr(dentry, "authenticator_"))
		{
			nump = strchr(dentry, '_') + 1;
			num = g_ascii_strtoll(nump, NULL, 10);
			if (num > max_num)
				max_num = num;
		}
	}
	return max_num;
}

int	authenticate_with_pam(query_t *q, char **reply)
{
	pam_handle_t	*pamh;
	struct pam_conv	plug = {misc_conv, NULL};
	int		retval;
	gchar		*conf_file;
	gchar		*check;
	
	check = g_strdup_printf("/etc/pam.d/%s.%s.conf", q->user_name, q->app_name);
	if (g_access(check, F_OK))
	{
		g_free(check);
		return UNREGISTRED_USER;
	}
	g_free(check);
	conf_file = g_strdup_printf("%s.%s.conf", q->user_name, q->app_name);

	retval = pam_start(conf_file, q->user_name, &plug, &pamh);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_start: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}
	g_free(conf_file);
	pam_get_item(pamh, PAM_SERVICE, &conf_file);
	g_print("[server] conf file for authenticaton: %s\n", conf_file);

	retval = pam_authenticate(pamh, 0);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_auth: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}

	g_print("[server] pam authentication passed with %s\n", retval == PAM_SUCCESS? "SUCCESS" : "FAILURE");

	pam_end(pamh, retval);
	if (retval == PAM_SUCCESS)
		return (OK);
	else
		return (INTERNAL_ERR);
}

int	gen_user_pam_files(query_t *q)
{
	gchar	*filename = NULL;
	gchar	*file_content;
	gchar	*service_file = NULL;
	gchar	*service_content;
	gchar	*crop;
	gchar	*tmp;
	gsize	len;
	GError	*error = NULL;
	int	fd;

	service_file = g_strdup_printf("/etc/pam.d/authenticator_%d", q->authenticator_number);
	if (!g_file_get_contents(service_file, &service_content, NULL, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", service_file, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}
	
	tmp = g_strrstr(service_content, "[Authentication]");
	crop = strchr(tmp, '\n') + 1;
	tmp = strchr(crop, '#');
	*tmp = '\0';

	file_content = g_strdup_printf("#[Authentication]\n%s\nauth	required	pam_permit.so\n", crop);
	filename = g_strdup_printf("/etc/pam.d/%s.system.conf", q->user_name);
	g_print("[server] generating %s file...\n", filename);
	if (!g_file_set_contents(filename, file_content, -1, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", filename, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}

	g_free(filename);
	filename = g_strdup_printf("/etc/pam.d/%s.default.conf", q->user_name);
	g_print("[server] generating %s file...\n", filename);
	if (!g_file_set_contents(filename, file_content, -1, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", filename, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}

	g_free(filename);
	g_free(service_file);
	g_free(service_content);
	g_free(file_content);

	return (OK);
}

int	enroll_with_pam(query_t *q, char **reply)
{
	pam_handle_t	*pamh;
	struct pam_conv	plug = {misc_conv, NULL};
	gchar		*service_file;
	int		retval;

	if (q->authenticator_number < 0 || q->authenticator_number > get_auth_count())
		return BAD_AUTHENTICATOR_NUMBER;
	service_file = g_strdup_printf("authenticator_%d", q->authenticator_number);

	g_print("[server] service file for enrollment: '%s'\n", service_file);
	retval = pam_start(service_file, q->user_name, &plug, &pamh);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_start: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}

	pam_putenv(pamh, "OP=ENROLL");
	retval = pam_chauthtok(pamh, 0);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_chauthtok: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}

	g_print("[server] pam enrollment passed with %s\n", retval == PAM_SUCCESS? "SUCCESS" : "FAILURE");

	gen_user_pam_files(q);

	pam_end(pamh, retval);
	g_free(service_file);
	if (retval == PAM_SUCCESS)
		return (OK);
	else
		return (INTERNAL_ERR);
}

int	change_authtok_with_pam(query_t *q, char **reply)
{
	pam_handle_t	*pamh;
	struct pam_conv	plug = {misc_conv, NULL};
	gchar		*service_file;
	int		retval;
	gchar		*check;
	
	check = g_strdup_printf("/etc/pam.d/%s.system.conf", q->user_name);
	if (g_access(check, F_OK))
	{
		g_print("user unregistred %s\n", check);
		g_free(check);
		return UNREGISTRED_USER;
	}
	g_free(check);
	if (q->authenticator_number < 0 || q->authenticator_number > get_auth_count())
		return BAD_AUTHENTICATOR_NUMBER;
	
	service_file = g_strdup_printf("authenticator_%d", q->authenticator_number);
	g_print("[server] service file for auth material change: '%s'\n", service_file);

	retval = pam_start(service_file, q->user_name, &plug, &pamh);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_start: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}

	pam_putenv(pamh, "OP=CHANGE");
	retval = pam_chauthtok(pamh, 0);
	if (retval != PAM_SUCCESS)
	{
		g_print("Error on pam_chauthtok: %s\n", pam_strerror(pamh, retval));
		return (INTERNAL_ERR);
	}

	g_print("[server] pam change authtok passed with %s\n", retval == PAM_SUCCESS? "SUCCESS" : "FAILURE");

	gen_user_pam_files(q);

	pam_end(pamh, retval);

	g_free(service_file);
	if (retval == PAM_SUCCESS)
		return (OK);
	else
		return (INTERNAL_ERR);

	return (0);
}

int	set_auth_state(query_t *q, char **reply)
{
	gchar	*conf_file;
	gchar	*auth_n;
	gchar	*conf_ctx;
	gchar	*ctx;
	gchar	*tail;
	char	*tmp;
	char	*crop;
	GError	*error = NULL;
	gchar		*check;
	
	check = g_strdup_printf("/etc/pam.d/%s.%s.conf", q->user_name, q->app_name);
	if (g_access(check, F_OK))
	{
		g_free(check);
		return UNREGISTRED_USER;
	}
	g_free(check);
	if (q->authenticator_number < 0 || q->authenticator_number > get_auth_count())
		return BAD_AUTHENTICATOR_NUMBER;
	
	conf_file = g_strdup_printf("/etc/pam.d/%s.%s.conf", q->user_name, q->app_name);
	auth_n = g_strdup_printf("/etc/pam.d/authenticator_%d", q->authenticator_number);
	if (!g_file_get_contents(auth_n, &ctx, NULL, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", auth_n, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}
	
	tmp = g_strrstr(ctx, "[Authentication]");
	crop = strchr(tmp, '\n') + 1;
	tmp = strchr(crop, '#');
	*tmp = '\0';

	if (!g_file_get_contents(conf_file, &conf_ctx, NULL, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", conf_file, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}

	if (q->state)
	{
		g_print("[server] setting authenticator to TRUE state for %s\n", q->user_name);
		tmp = g_strrstr(conf_ctx, "[Authentication]");
		tmp = strchr(tmp, '\n') + 1;
		tail = g_strdup(tmp);
		*tmp = '\0';
		tmp = g_strjoin(NULL, conf_ctx, crop, tail, NULL);
		g_free(tail);
	} else {
		g_print("[server] setting authenticator to FALSE state for %s\n", q->user_name);
		tmp = g_strrstr(conf_ctx, crop);
		tail = tmp + strlen(crop);
		*tmp = '\0';
		tmp = g_strjoin(NULL, conf_ctx, tail, NULL);
	}
	g_free(conf_ctx);

	if (!g_file_set_contents(conf_file, tmp, -1, &error))
	{
		g_printerr("Error on service_file '%s' reading, error:%s\n", conf_file, error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}
	g_free(tmp);
	g_free(ctx);
	g_free(conf_file);
	g_free(auth_n);

	return (OK);
}

int	get_auth_list(query_t *q, char **reply)
{
	GDir	*dir;
	GError	*error = NULL;
	gchar	*dentry;
	GTree	*desc_tree;
	gchar	*ctx;
	gchar	*crop;
	gchar	*nump;
	gint	num;
	gint	max_num = 0;
	gint	len = 0;
	gint	offset;

	dir = g_dir_open("/etc/pam.d/", 0, &error);
	if (dir == NULL)
	{
		g_printerr("Error on g_dir_open '/etc/pam.d/', error:%s\n", error->message);
		g_error_free(error);
		exit(EXIT_FAILURE);
	}
	
	g_print("[server] assembling authenticator list...");
	desc_tree = g_tree_new(strcmp);
	while ((dentry = g_dir_read_name(dir)))
	{
		gchar	*tmp = NULL;
		if (g_strrstr(dentry, "authenticator_"))
		{
			nump = strchr(dentry, '_') + 1;
			num = g_ascii_strtoll(nump, NULL, 10);
			if (num > max_num)
				max_num = num;
			dentry = g_strdup_printf("/etc/pam.d/%s", dentry);
			if (!g_file_get_contents(dentry, &ctx, NULL, &error))
			{
				g_printerr("Error on file_get_contents '%s', error:%s\n", dentry, error->message);
				g_error_free(error);
				exit(EXIT_FAILURE);
			}
			crop = g_strrstr(ctx, "[Description]");
			crop = strchr(crop, '#') + 1;
			tmp = strchr(crop, '\n');
			*tmp = '\0';
			g_strchug(crop);
			len += strlen(crop) + sizeof(gint);
			g_tree_insert(desc_tree, nump, g_strdup(crop));
			g_free(ctx);
			g_free(dentry);
		}
	}

	*reply = g_try_malloc0(len + sizeof(gint));
	num = 0;
	offset = 0;
	while(num <= max_num)
	{
		gchar *val;

		nump = g_strdup_printf("%d", num);
		val = (gchar *)g_tree_lookup(desc_tree, nump);
		len = strlen(val);
		memcpy(*reply + offset, &len, sizeof(len));
		offset += sizeof(len);
		memcpy(*reply + offset, val, len);
		offset += len;
		++num;
		g_print("Auth num:%s & desc:%s\n", nump, val);
		g_free(nump);
	}
	num = 0;
	memcpy(*reply + offset, &num, sizeof(num));

	g_tree_destroy(desc_tree);
	return (OK);
}

static gboolean	incoming_connection_cb(gint fd, GIOCondition cond, gpointer user_data)
{
	if (cond & (G_IO_ERR | G_IO_HUP | G_IO_NVAL))
		return FALSE;
	
	// accepting connection
	gint	client_fd;
	gint	rc;
	query_t	query = {};
	char	msgbuf[BUF_LEN];
	char	*reply;

	client_fd = accept(fd, NULL, NULL);

	g_print("[server] recieved bytes: %ld\n", recv(client_fd, msgbuf, sizeof(msgbuf), 0));
	
	parse_query(msgbuf, &query);

	switch (query.op)
	{
		case ENROLLMENT:
			g_print("[server] Enroll started...\n");
			rc = enroll_with_pam(&query, &reply);
			break;
		case AUTHENTICATION:
			g_print("[server] Authentication started...\n");
			rc = authenticate_with_pam(&query, &reply);
			break;
		case CHANGE_AUTHTOK:
			g_print("[server] Authentication material change started...\n");
			rc = change_authtok_with_pam(&query, &reply);
			break;
		case SET_AUTH_STATE:
			g_print("[server] Authenticator state change started...\n");
			rc = set_auth_state(&query, &reply);
			break;
		case GET_AUTH_LIST:
			g_print("[server] Generating authenticators list started...\n");
			rc = get_auth_list(&query, &reply);
			send(client_fd, reply, 1024, 0);
			close(client_fd);
			return (TRUE);
			break;
		case UNKNOWN:
			rc = UNKNOWNERR;
		default:
			rc = INTERNAL_ERR;
	}
	reply = g_try_malloc0(sizeof(rc));
	memcpy(reply, &rc, sizeof(rc));
	send(client_fd, reply, sizeof(rc), 0);
	
	g_free(reply);
	close(client_fd);
	return (TRUE);
}

static int	run_daemon(void)
{
	GSource		*conn_source	= NULL;
	server_t	server		= {};

	main_loop = g_main_loop_new(NULL, FALSE);

	setup_server(&server);
	// err check
	
	conn_source = g_unix_fd_source_new(server.sock_fd,
					   	G_IO_IN  |
					   	G_IO_ERR |
					   	G_IO_HUP |
					   	G_IO_NVAL);
	g_source_set_callback(conn_source, (GSourceFunc)incoming_connection_cb,
			      (gpointer)&server, NULL);
	g_source_attach(conn_source, NULL);
	// global-default context
	
	g_main_loop_run(main_loop);

	g_source_destroy(conn_source);
	g_source_unref(conn_source);
	close_server(&server);
	
	g_main_loop_unref(main_loop);
	return (0);
}

int	main(int argc, char **argv)
{


	return (run_daemon());
}
