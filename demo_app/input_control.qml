import QtQuick 2.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14

Page {
    id: controls
    visible: true
    width: 300
    height: 420
    title: qsTr("Passwd Input")

    Button {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.leftMargin: 5

        text: "Cancel"
        onClicked: {
            stack.pop()
        }
    }

    Label {
        id: error
        visible: false
        text: "Wrong password!"
        color: "red"
        anchors.bottom: pass_field.top
        anchors.bottomMargin: 20
        anchors.horizontalCenter: pass_field.horizontalCenter
    }

    TextField {
        id: pass_field
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 180
        placeholderText: "password"

    }

    GridLayout {
        id: grid; columns: 3
        anchors.bottom: parent.bottom

        Layout.bottomMargin: 5
        Layout.leftMargin: 5
        Layout.rightMargin: 5
        Button {
            text: "1"
            onClicked: {
                pass_field.text = pass_field.text + "1";
            }
        }
        Button {
            text: "2"
            onClicked: {
                pass_field.text = pass_field.text + "2";
            }
        }
        Button {
            text: "3"
            onClicked: {
                pass_field.text = pass_field.text + "3";
            }
        }
        Button {
            text: "4"
            onClicked: {
                pass_field.text = pass_field.text + "4";
            }
        }
        Button {
            text: "5"
            onClicked: {
                pass_field.text = pass_field.text + "5";
            }
        }
        Button {
            text: "6"
            onClicked: {
                pass_field.text = pass_field.text + "6";
            }
        }
        Button {
            text: "7"
            onClicked: {
                pass_field.text = pass_field.text + "7";
            }
        }
        Button {
            text: "8"
            onClicked: {
                pass_field.text = pass_field.text + "8";
            }
        }
        Button {
            text: "9"
            onClicked: {
                pass_field.text = pass_field.text + "9";
            }
        }
        Button {
            text: "Delete"
            onClicked: {
                error.visible = false
                pass_field.text = ""
            }
        }
        Button {
            text: "0"
            onClicked: {
                pass_field.text = pass_field.text + "0";
            }
        }
        Button {
            text: "Confirm"
            onClicked: {
                if (root.stored_pass === "")
                {
                    root.entered_pass = pass_field.text
                    stack.pop()
                }
                else if (root.stored_pass === pass_field.text)
                {
                    stack.push("notes_mainscreen.qml")
                }
                else
                {
                    error.visible = true
                }
            }
        }
    }

}
