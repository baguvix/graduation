#include <glib.h>
#include <gio/gio.h>
#include <dbus/dbus.h>
#include <stdio.h>
#include <stdlib.h>
//#include <dbus/dbus-internals.h>

int main(int argc, char **argv)
{

	char	*username = argv[1];
	int	rc;
	
/*	GDBusConnection	*conn;
	GDBusProxy	*proxy;
	GVariant	*reply;
	GError		*error = NULL;

	conn = g_dbus_connection_new_for_address_sync("unix:abstract=auth_bus", G_DBUS_CONNECTION_FLAGS_NONE, 
							NULL, NULL, &error);
	if (conn == NULL)
        {
                g_printerr ("Error connecting to D-Bus address %s: %s\n", "unix:aaaa....", error->message);
                g_error_free (error);
                return 1;
        }
	
	reply = g_dbus_connection_call_sync(conn, NULL, "/passwd", "ru.omprussia.passwd", "authenticate",
                                                g_variant_new("(s)", username), G_VARIANT_TYPE("(i)"),
                                                G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (reply == NULL)
        {
                g_printerr("Error invoking authenticate(string): %s\n", error->message);
                g_error_free(error);
                return 1;
        }
        g_variant_get(reply, "(i)", &rc);
        g_print("REPLY VALUE: %d\n", rc);
        if (rc)
        {
                g_print("Authentication success");
                return 0;
        }
*/
	DBusConnection	*conn;;
	DBusMessage	*msg;
	DBusMessage	*reply;
	DBusMessageIter	iter;
	DBusError	error;

	dbus_error_init (&error);
	conn = dbus_connection_open("unix:abstract=auth_bus", &error);
	if (conn == NULL)
	{
		fprintf (stderr, "Failed to open connection to 'auth_bus' message bus: %s\n",
			error.message);
		dbus_error_free (&error);
		exit (1);
	}

	msg = dbus_message_new_method_call(NULL,
                                           "/passwd",
                                           "ru.omprussia.passwd",
                                           "authenticate");
	  if (msg == NULL)
    {
      fprintf (stderr, "Couldn't allocate D-Bus message\n");
      exit (1);
    }

	dbus_message_iter_init_append(msg, &iter);
	dbus_message_iter_append_basic(&iter, DBUS_TYPE_STRING, &username);
	
	reply = dbus_connection_send_with_reply_and_block (conn,
                                                         msg, -1,
                                                         &error);
	      if (dbus_error_is_set (&error))
        {
          fprintf (stderr, "Error %s: %s\n",
		   error.name,
                   error.message);
          exit (1);
        }

	if (reply)
	{
		dbus_message_iter_init(reply, &iter);
		dbus_message_iter_get_basic(&iter, &rc);
		printf("got reply: %s\n", rc ? "OK" : "NOK");
	}
	return 1;
}
