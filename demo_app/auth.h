#ifndef AUTH_H
#define AUTH_H

#include <QQuickItem>
#include <QDir>
#include "auth_api.h"

class AuthManager : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(AuthManager)

public:
    explicit AuthManager(QQuickItem *parent = nullptr);
    ~AuthManager() override;

    Q_INVOKABLE void authenticate(void);
    Q_INVOKABLE void enrollment(qint32);

signals:
    void    authenticatioEnded(int status);
    void    enrollmentEnded(int status);

private:
    QString store;
};

#endif // AUTH_H
