#ifndef AUTH_MANAGER_PLUGIN_H
#define AUTH_MANAGER_PLUGIN_H

#include <QQmlExtensionPlugin>

class AuthManagerPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // AUTH_MANAGER_PLUGIN_H
